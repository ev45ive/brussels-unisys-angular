import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsComponent } from './playlists/views/playlists/playlists.component';
import { MusicSearchComponent } from './music/views/music-search/music-search.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'playlists', pathMatch: 'full'
  },
  {
    path: 'playlists',
    component: PlaylistsComponent
  },
  {
    path: 'music',
    component: MusicSearchComponent
  },
  {
    path: '**',
    redirectTo: 'playlists', pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: true,
    // useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
