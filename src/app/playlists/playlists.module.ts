import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsComponent } from './views/playlists/playlists.component';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { ListItemComponent } from './components/list-item/list-item.component';

// ng g c playlists/views/playlists

// ng g c playlists/components/playlists-list
// ng g c playlists/components/playlist-details
// ng g c playlists/components/list-item

import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    ListItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    PlaylistsComponent
  ]
})
export class PlaylistsModule { }
