import { Component, OnInit } from '@angular/core';
import { Playlist } from 'src/app/models/playlist';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular Hits!',
      favorite: true,
      color: '#ff00ff'
    }, {
      id: 234,
      name: 'Test 234',
      favorite: false,
      color: '#ffff00'
    }, {
      id: 345,
      name: 'Test 345',
      favorite: true,
      color: '#00ffff'
    }
  ]
  selected: Playlist | null = null

  constructor() { }

  save(draft: Playlist) {
    const index = this.playlists.findIndex(p => p.id == draft.id)
    if (index !== -1) {
      this.playlists.splice(index, 1, draft)
      this.selected = draft;
    }
  }

  ngOnInit() {
  }

}
