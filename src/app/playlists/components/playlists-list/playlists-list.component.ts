import { Component, OnInit, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
import { Playlist } from 'src/app/models/playlist';
import { NgForOfContext } from '@angular/common';

NgForOfContext

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
})
export class PlaylistsListComponent implements OnInit {

  @Input('items')
  playlists: Playlist[]

  @Input()
  selected: Playlist | null = null

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  select(select: Playlist) {
    this.selectedChange.emit(select == this.selected ? null : select)
  }

  trackFn(item: Playlist, index: number) {
    return item.id
  }

  constructor() { }

  ngOnInit() {
  }

}
