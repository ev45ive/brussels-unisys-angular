import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from 'src/app/models/playlist';

enum MODES { show = 'show', edit = 'edit' }

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist: Playlist

  MODES = MODES;
  mode: MODES = MODES.show

  constructor() { }

  ngOnInit() {
  }

  edit() {
    this.mode = MODES.edit
  }

  cancel() {
    this.mode = MODES.show
  }

  @Output()
  saveDraft = new EventEmitter<Playlist>()

  submit(value: Partial<Playlist>) {
    const draft = {
      ...this.playlist,
      ...value
    }
    this.saveDraft.emit(draft)
    this.mode = MODES.show
  }

}
