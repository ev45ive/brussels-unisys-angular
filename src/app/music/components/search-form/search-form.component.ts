import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { filter, distinctUntilChanged, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
})
export class SearchFormComponent implements OnInit {

  censor: ValidatorFn = function (control: AbstractControl): ValidationErrors | null {
    const error = (control.value as string).includes('batman')

    return error ? { 'censor': { word: 'batman' } } : null
  }

  queryForm = new FormGroup({
    query: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      this.censor
    ]),
    options: new FormGroup({
      type: new FormControl('album'),
    })
  })

  constructor() {
    console.log(this.queryForm)

    this.queryForm.get('query').valueChanges
      .pipe(
        debounceTime(400),
        filter(query => query.length >= 3),
        distinctUntilChanged(),
      )
      .subscribe(query => this.search(query))
  }

  @Output()
  queryChange = new EventEmitter<string>()

  search(query: string) {
    this.queryChange.emit(query)
  }


  ngOnInit() {
  }

}
