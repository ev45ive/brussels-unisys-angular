import { Injectable, Inject, InjectionToken } from '@angular/core';
import { Album, AlbumsResponse } from 'src/app/models/album';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/core/security/auth.service';

export class BaseSearch {
  constructor(protected http: HttpClient) { }
}

export const MUSIC_API_URL = new InjectionToken('API Url for Music Search Service')

@Injectable({
  providedIn: 'root'
  // providedIn: MusicModule
})
export class SearchService extends BaseSearch {

  albums: Album[] = [
    {
      id: '123',
      name: 'Service Cat',
      artists: [],
      images: [{
        url: 'http://placekitten.com/300/300',
        width: 300,
        height: 300
      }]
    }
  ]

  constructor(
    @Inject(MUSIC_API_URL) private api_url: string,
    protected http: HttpClient,
    private auth: AuthService) {
    super(http)
  }

  getAlbums(query = 'batman')/* : Observable<Album[]> */ {
    return this.http.get<AlbumsResponse>(this.api_url, {
      headers: {
        'Authorization': `Bearer ${this.auth.getToken()}`
      },
      params: {
        type: 'album', q: query
      },
    })
      .pipe(
        map(resp => resp.albums.items),
        // map( albums => albums),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            if (err.status == 401) {
              this.auth.authorize()
            }
          }
          return throwError(new Error(err.error.error.message))
          // return [] 
          // return empty()
          // return of([])
        })
      )
  }
}
import { Observable, empty, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators'
