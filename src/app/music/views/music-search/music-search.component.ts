import { Component, OnInit, Inject, Input } from '@angular/core';
import { Album } from 'src/app/models/album';
import { SearchService } from '../../services/search.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, concatMap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {

  albums: Album[] = []
  query = ''

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: SearchService
  ) { }

  search(query: string) {
    this.router.navigate([/* '/music' */], {
      queryParams: { q: query },
      relativeTo: this.route,
      replaceUrl: true
    })
  }

  ngOnInit() {
    this.route.queryParamMap
      .pipe(
        map(paramMap => paramMap.get('q')),
        map(query => query || 'batman'),
        // mergeMap(query => this.service.getAlbums(query)),
        // concatMap(query => this.service.getAlbums(query)),
        switchMap(query => this.service.getAlbums(query)),
        o => o
      )
      .subscribe(
        albums => this.albums = albums,
        error => console.error(error)
      )
  }

}
