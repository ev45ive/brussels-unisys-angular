import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { MusicSearchComponent } from './views/music-search/music-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { environment } from 'src/environments/environment';
import { SearchService, MUSIC_API_URL } from './services/search.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
// ng g m music -m app --routing true
// --
// ng g c music/view/music-search --export
// --
// ng g c music/component/search-form
// ng g c music/component/search-results
// ng g c music/component/album-card

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MusicRoutingModule,
    ReactiveFormsModule
  ],
  exports: [
    MusicSearchComponent
  ],
  providers: [
    {
      provide: MUSIC_API_URL,
      useValue: environment.music_api_url
    },
    // {
    //   provide: 'MusicService',
    //   useFactory: function (url: string) {
    //     return new SearchService(url)
    //   },
    //   deps: ['MUSIC_API_URL']
    // },
    // {
    //   provide: AbstractSearchService,
    //   useClass: SporifySearchService,
    //   // deps: ['MUSIC_API_URL']
    // },
    // {
    //   provide: SearchService,
    //   useClass: SearchService,
    // },
    // SearchService
  ]
})
export class MusicModule { }
