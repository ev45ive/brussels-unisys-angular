export interface Entity {
    id: string
    name: string
}

export interface Album extends Entity {
    artists: Artist[],
    images: AlbumImage[],
}
export interface Artist extends Entity { }
export interface AlbumImage {
    url: string;
    height: number;
    width: number;
}

export interface PagingObject<T> {
    items: T[]; offset: number; total: number
}

export interface AlbumsResponse {
    albums: PagingObject<Album>
}