export interface Entity {
    id: number;
    name: string;
}

export interface Playlist extends Entity {
    favorite: boolean;
    color: string;
    /**
     * Tracks Docs
     */
    tracks?: Track[]
    // tracks: Array<Track>
}

export interface Track extends Entity { };

// const x: Track = {
//     id:123,name:'set'
// }

// export class Track {
//     dosmoth(){}
// }
// instanceof Track;
// new Track()