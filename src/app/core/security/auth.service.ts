import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

export class AuthConfig {
  auth_url: string
  client_id: string
  response_type: string
  redirect_uri: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private config: AuthConfig
  ) {
    this.token = JSON.parse(sessionStorage.getItem('token'))

    if (location.hash && !this.token) {
      const p = new HttpParams({
        fromString: location.hash
      })
      this.token = p.get('#access_token')
      if ('string' == typeof this.token) {
        location.hash = ''
        sessionStorage.setItem('token', JSON.stringify(this.token))
      }
    }

  }

  authorize() {
    const { auth_url, client_id, redirect_uri, response_type } = this.config

    const p = new HttpParams({
      fromObject: { client_id, redirect_uri, response_type }
    })

    sessionStorage.removeItem('token')
    
    const url = `${auth_url}?${p.toString()}`
    location.href = (url)
  }

  private token: string | null = null

  getToken() {
    if (!this.token) {
      this.authorize()
    }
    return this.token
  }
}
