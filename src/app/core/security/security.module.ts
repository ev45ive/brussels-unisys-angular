import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { AuthConfig, AuthService } from './auth.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: AuthConfig,
      useValue: environment.auth_config
    }
  ]
})
export class SecurityModule {
  constructor(private auth: AuthService) {
    this.auth.getToken()
  }
}
