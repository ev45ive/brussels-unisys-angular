import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// angular.bootstrap(document.body, ['my-app'])

platformBrowserDynamic().bootstrapModule(AppModule, {
  // ngZone:'noop'
})
  .catch(err => console.error(err));




// AppComopnent.$Inject = ['asd']
// angular.component('name',AppComopnent)