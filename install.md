`install git,
install nodejs

git clone https://bitbucket.org/ev45ive/brussels-unisys-angular.git

cd brussels-unisys-angular
npm install

--- and ---

npm run serve

--- or ---

npm install -g @angular/cli
ng serve